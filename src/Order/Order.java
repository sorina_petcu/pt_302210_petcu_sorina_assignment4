package Order;

import java.util.Date;

public class Order {
	
	private int OrderId;
	private Date date;
	private int table;
	
	public Order(int OrderId, Date date, int table)
	{
		this.OrderId = OrderId;
		this.date = date;
		this.table = table;
	}
	
	@Override
	public int hashCode()
	{
		int rez = 1;
		final int prim = 29;
		rez = rez * prim + OrderId;
		
		if (date != null) 
			rez = rez * prim + date.hashCode();
		else
			rez = rez * prim;
		
		rez = rez * prim + table;
		return rez;		
	}

	public int getOrderId() {
		return OrderId;
	}

	public void setOrderId(int orderId) {
		OrderId = orderId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return true;
		
		Order next = (Order) obj;
		
		if (table != next.table)
			return false;
		if (OrderId != next.OrderId)
			return false;
		if (date == null)
		{
			if(next.date != null)
				return false;
		}
		
		else if(!date.equals(next.date))
			return false;
		
		return true;
	}

}
