package Order;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class BaseProduct extends MenuItem {
	
	private int id = 0;
	private String name;
	private double price;
	
	public BaseProduct(int id, String name, double price)
	{
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	public BaseProduct()
	{
		this.setId(0);
		this.setName(null);
		this.setPrice(0);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public double computePrice()
	{
		return this.getPrice();
	}
	
}
