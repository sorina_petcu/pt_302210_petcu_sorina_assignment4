package Order;

import java.util.List;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
	
	private int id;
	private String name;
	private double price;
	private List<MenuItem> products = new ArrayList<MenuItem>();
	
	public CompositeProduct(int id, String name)
	{
		this.setId(id);
		this.price = 0;
		this.name = name;
	}
	
	@Override
	public double computePrice()
	{
		double suma = 0;
		for(MenuItem item : products)
		{
			suma = suma + ((BaseProduct)item).computePrice();
		}
		return suma;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MenuItem> getProducts() {
		return products;
	}

	public void setProducts(List<MenuItem> products) {
		this.products = products;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int compareTo(CompositeProduct cp)
	{
		return (cp.id - this.id);
	}
	
	@Override
	public String toString()
	{
		System.out.println("Meniu : " + this.name);
		System.out.println("Continut Meniu : ");
		for(MenuItem item : getProducts())
		{
			System.out.println("Nume : " + ((BaseProduct)item).getName() + " " + "Pret : " + ((BaseProduct)item).getPrice()); 
		}
		
		System.out.println("Pret meniu : " + this.computePrice());
		
		return null;
	}
	
	public void add(MenuItem item)
	{
		products.add(item);
	}
	
	public void remove(MenuItem item)
	{
		products.remove(item);
	}
}
