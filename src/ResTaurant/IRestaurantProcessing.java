package ResTaurant;

public interface IRestaurantProcessing {
	
	public void CreateMenuItem();
	public void EditMenuItem();
	public void DeleteMenuItem();
	public void CreateNewOrder();
	public void ComputePrice();
	public void GenerateBill();

}
