package ResTaurant;

import Order.Order;
import Order.MenuItem;
import Order.CompositeProduct;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Date;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class Restaurant {
	
	private int id = 0;
	private HashMap<Order, HashSet<MenuItem>> database;
	
	public Restaurant()
	{
		database = new HashMap<Order, HashSet<MenuItem>>();
	}
	
	public HashMap<Order, HashSet<MenuItem>> getData()
	{
		return database;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	private boolean isWellFormed()
	{
		if(database == null)
			return false;
		return true;
	}
	
	public static void infoBox(String title, String infoMessage)
	{
		JOptionPane.showMessageDialog(null, title, infoMessage, JOptionPane.INFORMATION_MESSAGE);
	}

	public void addOrder(int id, int table, Date date)
	{ 
		assert (table >= 0 && id >=0): "Can't add order with a table/id negative!";
		int preSize = database.size();
		if (isWellFormed()) //class utf8
		{
			Order o = new Order(id, date, table);
			if (database.containsKey(o))
				infoBox("Order error:", "Add Order error");
			else
			{
				database.put(o,  new HashSet<MenuItem>());
				int postSize = database.size();
				assert (postSize == preSize + 1) : "It is not valid!";				
			}
						
		}
	}
	
	public void removeOrder(int id, int table, Date date)
	{
		assert (table >= 0 && id >=0): "Can't add order with a table/id negative!";
		int preSize = database.size();
		if (isWellFormed()) //class utf8
		{
			Order o = new Order(id, date, table);
			if (database.containsKey(o))
			{
				database.remove(o);
				int postSize= database.size();
				assert (postSize == preSize + 1) : "Not valid!";
				
			}	
			else
			
				infoBox("Remove order", "The order does not exist!");
								
			}
		    			
		}
	
	
	public void addMenu(Order order, CompositeProduct cp)
	{
		assert(cp != null && order != null) : "Cannot add Menu!";
		if(isWellFormed())
		{
			if(database.containsKey(order))
			{
				HashSet<MenuItem> meniu = (HashSet<MenuItem>) database.get(order);
				int preSize = database.size();
				meniu.add(cp);
				int postSize = database.size();
				assert(postSize == preSize + 1): "Not valid!";
				setId(getId() + 1);
				database.put(order, meniu);
			}
				
		}
		else
			infoBox("Add Menu error", "Menu does not exist!");
	}
	
	public Order getOrder(int table)
	{
		if(isWellFormed())
	 {
			Iterator<Order> i = database.keySet().iterator();
			while(i.hasNext())
			{
				if(i.next().getTable() == table)
				{
					return i.next();
				}
			}
	
	}
		return null;
	}

public HashSet<MenuItem> getMenu(Order o){
		
		if(isWellFormed()) {
			if(database.containsKey(o)) {
				return database.get(o);
			}
		}
		
		return null;
	}
	
}
