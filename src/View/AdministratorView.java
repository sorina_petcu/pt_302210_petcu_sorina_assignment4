package View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import com.sun.tools.javac.util.List;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
//import java.io.PrintWriter;
import java.awt.event.MouseAdapter;
import java.awt.GridLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Collection;

import Order.BaseProduct;
import Order.CompositeProduct;
//import ResTaurant.Restaurant;
import View.WaiterView.Increment;

@SuppressWarnings("unused")
public class AdministratorView extends JPanel {
	
	private JButton createBaseProduct = new JButton ("Create base product");
	private JButton createMenuItem = new JButton ("Create MenuItem");
	private JButton showMenuItem = new JButton ("Show MenuItem"); 
	private JButton showBaseProduct = new JButton ("Show base product");
	private JButton edit = new JButton ("Edit menu");
	private JButton delete = new JButton ("Delete menu");
	private JButton but = new JButton ("Back");
	
	private JTextField baseProduct = new JTextField ("");
	private JTextField meniuId = new JTextField ("");
	
	private JLabel basePr = new JLabel ("BaseProductId : ");
	private JLabel menu= new JLabel ("MenuId : ");
	
	private static final long serialVersionUID = 1L;
	private JFrame frame = new JFrame("Administrator duties");
	private ArrayList<BaseProduct> products;
	private List<CompositeProduct> cp = new ArrayList<CompositeProduct>();
	private Increment incr = new Increment();
	
	public class Increment
	{
		public int id = 1;
		public Increment() { 			
		}
	}
	
	public class nmbExtract
	{
		private ArrayList<Integer> integers = new ArrayList<Integer>();
		
		public ArrayList<Integer> getIntegers()
		{
			return integers;
		}
		public nmbExtract(String str)
		{
			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(str);
			while(m.find())
			{
				integers.add(Integer.parseInt(m.group()));
			}
	
	    }
	}
	
	public AdministratorView(ArrayList<BaseProduct> products)
	{
		
		this.products = products;
		setLayout(new GridLayout(3,2));
		add(basePr);
		add(baseProduct);
		add(menu);
		add(meniuId);
		//add(new JLabel(""));
		add(showBaseProduct);
		add(createBaseProduct);
		add(createMenuItem);
		add(edit);
		add(delete);
		add(but);
		
		frame.setSize(600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		showBaseProduct();
		createMenuItem();
		showMenuItem();
		editMenu();
		butListener();
		setVisible(true);
		
		frame.add(this);
			
	}
	
	private void butListener() 
	{
		but.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e)
			{
				frame.setVisible(false);
			}
		});
	}
	
	public void createListener()
	{
		createBaseProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
			}
		});
	}
	
	public List<CompositeProduct> getComposite()
	{
		return cp;
	}

	private void editMenu() {
		// TODO Auto-generated method stub
		edit.addMouseListener(new MouseAdapter()
				{
			       
			       @Override
			       public void mouseReleased(MouseEvent e)
			       {
			    	   String s = baseProduct.getText();
			    	   String name = "";
			    	   int menuId = Integer.parseInt(meniuId.getText());
			    	   nmbExtract number = new nmbExtract(s);
			    	   System.out.println(number.getIntegers());
			    	   
			    	   for(Integer i : number.getIntegers())
			    	   {
			    		   name += products.get(i-1).getName();
			    		   name += " ,";
			    	   }
			    	   
			    	  System.out.println(name);
			    	  
			    	  CompositeProduct cPr = new CompositeProduct(menuId, name);
			    	  for(Integer i : number.getIntegers())
			    	  {
			    		  cPr.add(products.get(i-1));
			    	  }
			    	  
			    	  for(CompositeProduct cProduct: cp)
			    	  {
			    		  if(cProduct.getId() == menuId)
			    		  {
			    			  cp.remove(cProduct);
			    		  }
			    	  }
			    	  
			    	  cp.add(cPr);
			    	  meniuId.setText("");
			    	  baseProduct.setText("");
			    	  
			       }
				});
	}

	private void showMenuItem() {
		showMenuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e)
			{
				new MenuItemView(cp);
			}
		});
		
	}

	private void createMenuItem() {
		// TODO Auto-generated method stub
		createMenuItem.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e)
			{
				   String s = baseProduct.getText();
		    	   String name = "";
		    	   //int menuId = Integer.parseInt(meniuId.getText());
		    	   nmbExtract number = new nmbExtract(s);
		    	   System.out.println(number.getIntegers());
		    	   
		    	   for(Integer i : number.getIntegers())
		    	   {
		    		   name += products.get(i-1).getName();
		    		   name += " ,";
		    	   }
		    	   
		    	  System.out.println(name);
		    	  
		    	  CompositeProduct cPr = new CompositeProduct(incr.id++, name);
		    	  for(Integer i : number.getIntegers())
		    	  {
		    		  cPr.add(products.get(i-1));
		    	  }
		    	  
		    	  cp.add(cPr);
		    	  //meniuId.setText("");
		    	  baseProduct.setText("");
			}
			
		});
		
	}

	private void showBaseProduct() {
		showBaseProduct.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e)
			{
				new BaseProductView(products);
			}
		});
		
	}
	
	public void Delete(final ActionListener actionListener)
	{
		delete.addActionListener(actionListener);
	}
	
}
