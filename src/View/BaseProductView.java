package View;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.GridLayout;

import Order.BaseProduct;

public class BaseProductView extends JPanel {
	
	String data;
	ArrayList<BaseProduct> products;
	DefaultTableModel defaultTM =  new DefaultTableModel();
	
	private JTable table;
	private JFrame frame;
	private JButton but;
	private static final long serialVersionUID = 1L;
	
	public void getData()
	{
		defaultTM.fireTableDataChanged();
		defaultTM.getDataVector().removeAllElements();
		
		Object columns[] = {"Id", "Name", "Price"};
		defaultTM.setColumnIdentifiers(columns);
		Object cdata[] = new Object[3];
		for(BaseProduct bp : products) 
		{
			System.out.println(bp.getId() + " " + bp.getName() + " " + bp.getPrice());
			
			cdata[0] = bp.getId();
			cdata[1] = bp.getName();
			cdata[2] = bp.getPrice();
			defaultTM.addRow(cdata);
			
		}
	}
	
	public BaseProductView(ArrayList<BaseProduct> products)
	{
		this.products = products;
		
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(true);
		setLayout(new GridLayout(2, 1));
		
		but = new JButton("Back");
		but.setBounds(0, 10, 0, 22);
		but.addMouseListener(new MouseAdapter()
		{
			public void mouseReleased(MouseEvent m)
			{
				frame.setVisible(false);
			}			
		});
		
		Object columns[] = {"Id", "Name", "Price"};
		defaultTM.setColumnIdentifiers(columns);
		JScrollPane pane = new JScrollPane();
		table = new JTable();
		table.setModel(defaultTM);
		pane.setViewportView(table);
		
		add(but);
		add(pane);
		frame.add(this);
		getData();
		
	}
	
}
