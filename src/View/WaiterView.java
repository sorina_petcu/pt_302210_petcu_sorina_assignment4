package View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import com.sun.tools.javac.util.List;
import java.awt.event.MouseEvent;
import java.io.PrintWriter;
import java.awt.event.MouseAdapter;
import java.awt.GridLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Date;

import Order.BaseProduct;
import Order.CompositeProduct;
import Order.MenuItem;
import Order.Order;
import ResTaurant.Restaurant;

@SuppressWarnings("unused")
public class WaiterView extends JPanel{
	
	private JButton createNewOrder = new JButton ("Create new order");
	private JButton computePrice = new JButton ("Compute price");
	private JButton generateBill = new JButton ("Generate bill"); //
	private JButton but = new JButton ("Back");
	
	private JTextField tableTxt = new JTextField ("");
	private JTextField meniuTxt = new JTextField ("");
	
	private JLabel tableLbl = new JLabel ("Table");
	private JLabel tableMeniu = new JLabel ("Meniu item");
	
	private static final long serialVersionUID = 1L;
	private JFrame frame = new JFrame("Administrator duties");
	private Restaurant restaurant = new Restaurant();
	private List<CompositeProduct> cp;
	private Increment incr = new Increment();
	
	public class Increment
	{
		public int id = 1;
		public Increment() { 			
		}
	}
	
	public WaiterView(List<CompositeProduct> products) 
	{
		this.cp = products;
		setLayout(new GridLayout(3,2));
		add(createNewOrder);
		add(computePrice);
		add(generateBill);
		add(new JLabel(""));
		add(tableLbl);
		add(tableTxt);
		add(tableMeniu);
		add(meniuTxt);
		add(but);
		
		frame.setSize(600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
				
		createNewOrder();
		computePrice();
		generateBill();
		butListener();
		setVisible(true);
		
		frame.add(this);	
	}
	
	public class nmbExtract
	{
		private ArrayList<Integer> integers = new ArrayList<Integer>();
		
		public ArrayList<Integer> getIntegers()
		{
			return integers;
		}
		public nmbExtract(String str)
		{
			Pattern p = Pattern.compile("\\d+");
			Matcher m = p.matcher(str);
			while(m.find())
			{
				integers.add(Integer.parseInt(m.group()));
			}
	
	    }
	}
	
	public void createNewOrder()
	{
		createNewOrder.addMouseListener(new MouseAdapter()
				{
			@Override
			public void mouseReleased(MouseEvent e)
			{
				int table = Integer.parseInt(tableTxt.getText());
				Order o = new Order(incr.id++, new Date(System.currentTimeMillis()),table);
				restaurant.addOrder(o.getOrderId(), o.getTable(), o.getDate());
				String str = meniuTxt.getText();
				nmbExtract number = new nmbExtract(str);
				System.out.println(number.getIntegers());
				
				for(Integer i : number.getIntegers())
				{
					restaurant.addMenu(o, cp.get(i-1));
				}
				
				tableTxt.setText("");
				meniuTxt.setText("");
			
			}
			
				});
	}
	
	public void computePrice()
	{
		
		computePrice.addMouseListener(new MouseAdapter() {

		@Override
		public void mouseReleased(MouseEvent e)
		{
			    PrintWriter ptwr;
				int table = Integer.parseInt(tableTxt.getText());
				
				try {
					Order o = restaurant.getOrder(table);
					if(o != null) {
						double sum = 0;
						HashSet<MenuItem> menu = restaurant.getMenu(o);
						Iterator<MenuItem> i = menu.iterator();
						while(i.hasNext()) {
							sum += ((CompositeProduct)i.next()).computePrice();
						}
						ptwr = new PrintWriter("bill.txt","UTF-8");
						ptwr.println(o.getOrderId() +" " + o.getTable() +" " + sum );
						ptwr.close();
						System.out.println("success!");
					}
					
				} catch (Exception e2) {
					
		}
  }
	
});
	}
	
	public void generateBill() {
		generateBill.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e)
			{
				new ChiefView(restaurant);
			}
		});
	}
	
	public void butListener()
	{
		but.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseReleased(MouseEvent e)
			{
				frame.setVisible(false);
			}
		});
	}
}
