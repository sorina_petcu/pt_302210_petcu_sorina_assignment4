package View;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

//import com.sun.tools.javac.util.List;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.GridLayout;

import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Date;

import Order.BaseProduct;
import Order.CompositeProduct;
import Order.MenuItem;
import ResTaurant.Restaurant;

@SuppressWarnings("unused")
public class ChiefView extends JPanel{
	
	private JTable table;
	private JFrame frame;
	private JButton but;
	private static final long serialVersionUID = 1L;
	
	String data;
	List<CompositeProduct> cp;
	DefaultTableModel defaultTM =  new DefaultTableModel();
	Restaurant restaurant;
	
	public void getData()
	{
		defaultTM.fireTableDataChanged();
		defaultTM.getDataVector().removeAllElements();
		
		Object columns[] = {"Id", "Table", "Price"};
		defaultTM.setColumnIdentifiers(columns);
		Object cdata[] = new Object[3];
		for(HashSet<MenuItem> mi : restaurant.getData().values()) 
		{
			Iterator<MenuItem> i  = mi.iterator();
			Object CompositeProduct;
			while(i.hasNext())
			//System.out.println(bp.getId() + " " + bp.getName() + " " + bp.getPrice());
			{
			CompositeProduct cp = (CompositeProduct) i.next();
			cdata[0] = cp.getId();
			cdata[1] = cp.getName();
			cdata[2] = cp.computePrice();
			defaultTM.addRow(cdata);
		    }
		}
	}
	
	public ChiefView (Restaurant restaurant)
	{
		this.restaurant = restaurant;
		
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(true);
		setLayout(new GridLayout(2, 1));
		but = new JButton("Back");
		but.setBounds(0, 10, 0, 22);
		but.addMouseListener(new MouseAdapter()
		{
			public void mouseReleased(MouseEvent m)
			{
				frame.setVisible(false);
			}			
		});
		
		Object columns[] = {"Id", "Table", "Price"};
		defaultTM.setColumnIdentifiers(columns);
		JScrollPane pane = new JScrollPane();
		table = new JTable();
		table.setModel(defaultTM);
		pane.setViewportView(table);
		
		add(but);
		add(pane);
		frame.add(this);
		getData();
		
	}
	

}
