package Main;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

public class Main extends JPanel {
	
	private JButton Administrator = new JButton("Admistrator duties");
	private JButton Chef = new JButton("Chef View");
	private JButton Waiter = new JButton("Waiter duties");
	private JFrame frame = new JFrame("Restaurant processing");
	private static final long serialVersionUID = 1L;
	
	public Main()
	{
		add(Administrator);
		add(Waiter);
		setVisible(true);
		frame.add(this);
		
		frame.setSize(600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		setLayout(new GridLayout(2,3));
	}
	
	public void AdministratorOperations(final ActionListener actionListener)
	{
		Administrator.addActionListener(actionListener);
	}
	
	public void WaiterOperations(final ActionListener actionListener)
	{
		Waiter.addActionListener(actionListener);
	}
	
	public void ChefOperations(final ActionListener actionListener)
	{
		Chef.addActionListener(actionListener);
	}

	
}
