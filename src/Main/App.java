package Main;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Date;
import Controller.Controller;
import Order.BaseProduct;

@SuppressWarnings("unused")
public class App {
	
	public static void main(String[] args)
	{
		class Increment
		{
			public int id = 1;
			public Increment() { 			
			}
		}
		
		Increment incr = new Increment();
		ArrayList<BaseProduct> products = new ArrayList<BaseProduct>();
		BaseProduct bp1 = new BaseProduct(incr.id++, "Brocolli", 10);
		BaseProduct bp2 = new BaseProduct(incr.id++, "Piept de pui", 40);
		BaseProduct bp3 = new BaseProduct(incr.id++, "Muschi de vita", 55);
		BaseProduct bp4 = new BaseProduct(incr.id++, "Spanac", 5);
		BaseProduct bp5 = new BaseProduct(incr.id++, "Rosii", 20);
		products.add(bp1);
		products.add(bp2);
		products.add(bp3);
		products.add(bp4);
		products.add(bp5);
		new Controller(products);
	
	}
}
