package Controller;

import java.util.ArrayList;
import java.util.List;

import Order.BaseProduct;
import Order.CompositeProduct;
import View.WaiterView;
import View.AdministratorView;
import Main.Main;

import javax.swing.JOptionPane;

@SuppressWarnings("unused")
public class Controller {
	
	ArrayList<BaseProduct> products;
	List<CompositeProduct> cp;
	Main main;
	AdministratorView adminV;
	
	public Controller(ArrayList<BaseProduct> products)
	{
		main = new Main();
		this.products = products;
		initializeActionListeners();
	}
	
	private void AdministratorOperations()
	{
		main.AdministratorOperations(e->{
			
			this.cp = new AdministratorView(products).getComposite();
			
		});
	}
	
	private void WaiterOperations()
	{
		main.WaiterOperations(e->{
			new WaiterView(cp);
		});
	}
	
	private void ChefOperations()
	{
		main.ChefOperations(e->{
		});
	}
	
	private void initializeActionListeners()
	{
		AdministratorOperations();
		WaiterOperations();
		ChefOperations();
		
	}

}
